
file { '/etc/motd':
	content => "

    Riak VM cluster node: $number
    - Version 1.1
    - OS: Ubuntu precise-server-cloudimg-amd64
    - IP:      $ipaddr
    - Code:    ~/code/source
\n"
}

# Update the latest ubuntu packages
#  exec { 'apt-get update':
#    command => '/usr/bin/apt-get update'
#  }

# Install curl
  package { "curl":
    ensure => "latest",
#  require    => Exec["apt-get update"],
  }

# Get the riak signing key
  exec { 'apt-key add':
    command => '/usr/bin/curl http://apt.basho.com/gpg/basho.apt.key | /usr/bin/sudo apt-key add - update',
    require    => Package["curl"],
  }

# Get the riak signing key
  exec { 'add repo':
    command => '/usr/bin/sudo /bin/bash -c "/bin/echo deb http://apt.basho.com $(lsb_release -sc) main > /etc/apt/sources.list.d/basho.list"',
    require    => Exec["apt-key add"],
  }

# Update the latest ubuntu packages
  exec { 'apt-get update2':
    command => '/usr/bin/apt-get update',
    require    => Exec["add repo"],
  }

# Install riak
  package { "riak":
    ensure => "latest",
    require => Exec["apt-get update2"],
  }

# Change app.config, so riak listens on public ip rather than just localhost
  file { 'app.config':
    path    => '/etc/riak/app.config',
    ensure  => file,
    require => Package["riak"],
    content => template('riak/app.config.erb'),
  }

# Change the node name, for clustering
  file { 'vm.args':
    path    => '/etc/riak/vm.args',
    ensure  => file,
    require => File["app.config"],
    content => template('riak/vm.args.erb'),
  }

# Start riak
  exec { 'start riak':
    command => '/usr/bin/sudo /usr/sbin/riak start',
    require => File["vm.args"],
  }

if $number == '1' {
  notice('I am Node 1 so will NOT try to cluster!')
} else {
  notice('I am NOT Node 1 so I will try to cluster to Node 1')
  # Cluster
  exec { 'cluster node':
    command => '/usr/bin/sudo /usr/sbin/riak-admin cluster join riak@192.168.33.111',
    require => Exec["start riak"],
  }
  if $number == $maxnode {
    notice('I am the last Node of a multi Node cluster, so time to plan and commit the clustering!')

    # Plan clustering
    exec { 'cluster plan':
      command => '/usr/bin/sudo /usr/sbin/riak-admin cluster plan',
      require => Exec["cluster node"],
    }

    # Commit clustering
    exec { 'cluster commit':
      command => '/usr/bin/sudo /usr/sbin/riak-admin cluster commit',
      require => Exec["cluster plan"],
    }
  }
}