case $maxnode {
  '1': {
    $servers = "    server riak-1.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net"
  }
  '2': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net"
  }
  '3': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n    server riak-3.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net\n192.168.33.113    riak-3.justtim.net"
  }
  '4': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n    server riak-3.justtim.net:8098;\n    server riak-4.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net\n192.168.33.113    riak-3.justtim.net\n192.168.33.114    riak-4.justtim.net"
  }
  '5': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n    server riak-3.justtim.net:8098;\n    server riak-4.justtim.net:8098;\n    server riak-5.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net\n192.168.33.113    riak-3.justtim.net\n192.168.33.114    riak-4.justtim.net\n192.168.33.115    riak-5.justtim.net"
  }
  '6': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n    server riak-3.justtim.net:8098;\n    server riak-4.justtim.net:8098;\n    server riak-5.justtim.net:8098;\n    server riak-6.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net\n192.168.33.113    riak-3.justtim.net\n192.168.33.114    riak-4.justtim.net\n192.168.33.115    riak-5.justtim.net\n192.168.33.116    riak-6.justtim.net"
  }
  '7': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n    server riak-3.justtim.net:8098;\n    server riak-4.justtim.net:8098;\n    server riak-5.justtim.net:8098;\n    server riak-6.justtim.net:8098;\n    server riak-7.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net\n192.168.33.113    riak-3.justtim.net\n192.168.33.114    riak-4.justtim.net\n192.168.33.115    riak-5.justtim.net\n192.168.33.116    riak-6.justtim.net\n192.168.33.117    riak-7.justtim.net"
  }
  '8': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n    server riak-3.justtim.net:8098;\n    server riak-4.justtim.net:8098;\n    server riak-5.justtim.net:8098;\n    server riak-6.justtim.net:8098;\n    server riak-7.justtim.net:8098;\n    server riak-8.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net\n192.168.33.113    riak-3.justtim.net\n192.168.33.114    riak-4.justtim.net\n192.168.33.115    riak-5.justtim.net\n192.168.33.116    riak-6.justtim.net\n192.168.33.117    riak-7.justtim.net\n192.168.33.118    riak-8.justtim.net"
  }
  '9': {
    $servers = "    server riak-1.justtim.net:8098;\n    server riak-2.justtim.net:8098;\n    server riak-3.justtim.net:8098;\n    server riak-4.justtim.net:8098;\n    server riak-5.justtim.net:8098;\n    server riak-6.justtim.net:8098;\n    server riak-7.justtim.net:8098;\n    server riak-8.justtim.net:8098;\n    server riak-9.justtim.net:8098;\n"
    $hsts = "192.168.33.111    riak-1.justtim.net\n192.168.33.112    riak-2.justtim.net\n192.168.33.113    riak-3.justtim.net\n192.168.33.114    riak-4.justtim.net\n192.168.33.115    riak-5.justtim.net\n192.168.33.116    riak-6.justtim.net\n192.168.33.117    riak-7.justtim.net\n192.168.33.118    riak-8.justtim.net\n192.168.33.119    riak-9.justtim.net"
  }
  default: { notice('You seem to want more than 9 nodes. Maybe think again??') }
}

file { '/etc/hosts':
    path => "/etc/hosts",
    content => template('nginx/hosts.erb'),  
}

file { '/etc/motd':
	content => "

    nginx Load balancing proxy VM
    - Version 1.1
    - OS: Ubuntu precise-server-cloudimg-amd64
    - IP:      192.168.33.128
\n",
}

# Update the latest ubuntu packages
  exec { 'apt-get update':
    command => '/usr/bin/apt-get update',
    require => File['/etc/hosts'],
  }

# Install nginx
  package { "nginx":
    ensure => "latest",
    require => Exec["apt-get update"],
  }

# Edit the nginx configuration.
  file { 'nginx-conf':
    path => "/etc/nginx/sites-enabled/default",
    content => template('nginx/default.erb'),
    require => Package["nginx"],
    notify => Service[nginx],
  }

# Restart nginx
  service { 'nginx':
    ensure => running,
    enable => true,
    hasrestart => true,
    require => Package["nginx"],
  }