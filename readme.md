#RiakCluster

Vagrant setup for a load balanced riak cluster

## Instructions

- Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant 1.3.5](http://downloads.vagrantup.com/tags/v1.3.5)
- Install the [vagrant-vbguest plugin](https://github.com/dotless-de/vagrant-vbguest): `vagrant plugin install vagrant-vbguest`
- Clone this repo `git clone git@bitbucket.org:TimCoatesNHS/riakcluster.git`
- Change into riakcluster directory
- Optionally edit the `Vagrantfile` to:
-- Set the number of nodes you want to run (max 9) on line 9
-- Change between bitcask (riak's default) and eLevelDB (which allows secondary indexing) backend storage engines.

## Spin the cluster up...
vagrant up (be prepared for a wait!)   
You can also just 'up' one specific machine eg:
- vagrant up riak-1.justtim.net
- vagrant up nginx.justtim.net

## Test the cluster
curl -XPUT http://192.168.33.128:8098/riak/images/1.jpg -H "Content-type: image/jpeg" --data-binary @test.jpg   
This will put the supplied test.jpg image into the riak clustered storage.   
Browsing to http://192.168.33.128:8098/riak/images/1.jpg should retrieve it from nginx load balancing your riak nodes   
Browsing to http://192.168.33.121:8098/riak/images/1.jpg will retrieve it from node 1   

## Notes
The nginx vm runs on 192.168.33.128   

The nginx rules are (I think) the miminum necessary to load balance. It listens on port 8098, and round robins the requests onwards to the riak nodes.

The riak nodes run on 192.168.33.111,2,3... Etc   

## To Login to each vm
vagrant ssh nginx.justtim.net   
vagrant ssh riak-1.justtim.net   
vagrant ssh riak-2.justtim.net   
You probably get the picture

## ToDo
- Make the way multiple entries in the nginx config are written out. Currently has a ridiculous amount of duplication.
- Ditto for the hosts file (on the nginx box, to add a reference for the riak hosts).
- Perhaps find a way to cache the packages we download, so multiple downloads are faster. Maybe nginx can be employed to do this.
- Make a single node option run nginx on the first node, to reduce the resoures used.
- Tune the RAM for minimal use, while retaining reasonable performance.
- Perhaps be a little defensive, so that for example bringing up Node 2 of a multi node cluster doesn't try and fail to cluster with Node 1 which may well not be up yet.

## Copyright & License

Released under GNU GPL version 2 and is therefore Free Software as defined by the Free Software Foundation. You may read the entire copy of the license in the LICENSE file distributed with this project
