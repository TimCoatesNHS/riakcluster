# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"
SourcePath = "./source"

##########################
# Number of nodes here...
nodes = 2

##########################
# Storage backend, must use one of the below
# riak_backend = "riak_kv_bitcask_backend"
riak_backend = "riak_kv_eleveldb_backend"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # choices for virtual machines:
  config.vm.box = 'precise64'
  config.vm.box_url = 'http://files.vagrantup.com/precise64.box'

  #config.vm.box = 'debian-6.0'
  #config.vm.box_url = 'http://puppetlabs.s3.amazonaws.com/pub/Squeeze64.box'

  #config.vm.box = 'CentOS-6.3_x86_64-small'
  #config.vm.box_url = 'https://1412126a-vagrant.s3.amazonaws.com/CentOS-6.3-x86_64-reallyminimal.box'

  #config.vm.box = "raring64"
  #config.vm.box_url = "http://cloud-images.ubuntu.com/vagrant/raring/current/raring-server-cloudimg-amd64-vagrant-disk1.box"

    # Share an additional folder to the guest VM. The first argument is
    # the path on the host to the actual folder. The second argument is
    # the path on the guest to mount the folder. And the optional third
    # argument is a set of non-required options.
    config.vm.synced_folder SourcePath, "/home/vagrant/code/source/", :nfs => true

    # VM fine tuning for VirtualBox:
    #
    config.vm.provider :virtualbox do |vb|
      # Uncomment the following line to have the vms not boot with headless mode
      # vb.gui = false
   
      # Use VBoxManage to customize the VM. For example to change memory:
      vb.customize ["modifyvm", :id, "--memory", "512"]
      # To add cores
      vb.customize ["modifyvm", :id, "--cpus", "2"]
      # To reduce assigned video memory
      vb.customize ["modifyvm", :id, "--vram", "2"]
      #vb.customize ["modifyvm", :id, "--ioapic", "on"]

      # Via http://blog.liip.ch/archive/2012/07/25/vagrant-and-node-js-quick-tip.html
      vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
    end

  baseip = 110
  (1..nodes).each do |n|
    # For each node we do the following...

    ip = "192.168.33.#{baseip + n.to_i}"
    name = "riak-#{n}.justtim.net"

    config.vm.define name do |cfg|

      # Create a private network, which allows host-only access to the machine
      # using a specific IP.
      cfg.vm.hostname = "riak-#{n}.justtim.net"
      cfg.vm.network :private_network, ip: "#{ip}"

      # Enable provisioning with Puppet stand alone, but first we need to set some variables for
      # Puppet to use.
      cfg.vm.provision :puppet do |puppet|
        puppet.facter = {
          "ipaddr" => "#{ip}",
          "number" => n,
          "maxnode" => nodes,
          "backend" => riak_backend,
        }
        puppet.manifests_path = "manifests"
        puppet.manifest_file  = "default.pp"
        puppet.module_path = "modules"
      end
    end
  end
  # End of the loop repeated for each riak node



  # Here we create an nginx to load balance our riak cluster
  config.vm.define "nginx.justtim.net" do |cfg|

    cfg.vm.hostname = "nginx.justtim.net"
    cfg.vm.network :private_network, ip: "192.168.33.128"

    # Enable provisioning with Puppet stand alone.
    cfg.vm.provision :puppet do |puppet|
      puppet.facter = {
        "maxnode" => nodes
      }
      puppet.manifests_path = "manifests"
      puppet.manifest_file  = "nginx.pp"
      puppet.module_path = "modules"
    end
  end

end